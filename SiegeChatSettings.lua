SiegeChatSettings = {}
SiegeChatSettings.Window = "SiegeChatFilters"
SiegeChatSettings.logName = "SiegeWeaponGeneralFireWindowChatLogDisplay"
SiegeChatSettings.channelListData = {}
SiegeChatSettings.channelListOrder = {}
SiegeChatSettings.channelFiltersState = {}

function SiegeChatSettings.WindowInit()
	CreateWindow(SiegeChatSettings.Window, true)
	WindowSetShowing(SiegeChatSettings.Window, false)
	-- Apply the general GUI settings
	ButtonSetText(SiegeChatSettings.Window.."AcceptButton", L"Accept")
	LabelSetText(SiegeChatSettings.Window.."TitleBarText" , L"Siege Chat")
	LabelSetText(SiegeChatSettings.Window.."Heading"      , L"Select Channels")
end

function SiegeChatSettings.ShowWindow(flags, mouseX, mouseY)
	SiegeChatSettings.InitChatOptionListData()
	SiegeChatSettings.FilterChannelList()
	ListBoxSetDisplayOrder( SiegeChatSettings.Window.."List", SiegeChatSettings.channelListOrder )
	SiegeChatSettings.UpdateChatOptionRow()
	WindowSetShowing(SiegeChatSettings.Window, true)
end

--[[
function SiegeChatSettings.OnShown()
	WindowUtils.OnShown()
	ResetChannelList()
	SiegeChatSettings.UpdateChatOptionRow()
end
--]]

function SiegeChatSettings.CloseWindow(flags, mouseX, mouseY)
	WindowSetShowing(SiegeChatSettings.Window, false )
end

function SiegeChatSettings.SaveOptions(flags, mouseX, mouseY)
	local isActive = false
	-- TODO: Could this for loop be replaced with an ipairs?  Means ChatSettings.Ordering[idx] => a
	for k, a in ipairs(ChatSettings.Ordering) do -- idx=1, #ChatSettings.Ordering do
		-- Check if options has changed and if so, update the filter 
		isActive = LogDisplayGetFilterState( SiegeChatSettings.logName, ChatSettings.Channels[a].logName, a)
		if isActive ~= SiegeChatSettings.channelFiltersState[k] then
			LogDisplaySetFilterState( SiegeChatSettings.logName, ChatSettings.Channels[a].logName, a, not isActive)
			WTes.Settings.SiegeChat.Channels[a] = not isActive
		end
	end
	SiegeChatSettings.CloseWindow(nil, nil, nil)
end

function SiegeChatSettings.InitChatOptionListData()
	SiegeChatSettings.channelListData = {}
	local channelIndex = 0
	for k, a in pairs(ChatSettings.Channels) do
		if a ~= nil and a.name ~= nil then
			channelIndex = channelIndex + 1
			SiegeChatSettings.channelListData[channelIndex] = {}
			SiegeChatSettings.channelListData[channelIndex].channelName = a.name
			SiegeChatSettings.channelListData[channelIndex].color       = ChatSettings.ChannelColors[k]
			SiegeChatSettings.channelListData[channelIndex].logName     = a.logName
			SiegeChatSettings.channelListData[channelIndex].channelID   = a.id
		end
	end
end

function SiegeChatSettings.FilterChannelList()
	-- Reset the tables
	SiegeChatSettings.channelListOrder = {}
	SiegeChatSettings.channelFiltersState = {}
	local isActive = false
	for k, a in ipairs (ChatSettings.Ordering) do
		for g, e in ipairs( SiegeChatSettings.channelListData ) do
			if a == e.channelID then
				table.insert(SiegeChatSettings.channelListOrder, g)
				isActive = LogDisplayGetFilterState(SiegeChatSettings.logName, e.logName, e.channelID)
				-- Cache off all of the values, for comparison later
				SiegeChatSettings.channelFiltersState[k] = isActive
				if k < SiegeChatFiltersList.numVisibleRows then
					-- Only fill the table with the number of Checkboxes actually visible
					ButtonSetPressedFlag(SiegeChatSettings.Window.."ListRow"..k.."CheckBox", isActive)
				end
				break
			end
		end
	end
end

-- Callback from the <List> that updates a single row.
function SiegeChatSettings.UpdateChatOptionRow()
	if SiegeChatFiltersList.PopulatorIndices ~= nil then
		local isActive = false
		for k, a in ipairs(SiegeChatFiltersList.PopulatorIndices) do
			for g, e in ipairs(ChatSettings.Ordering) do -- idx=1, #ChatSettings.Ordering do
				if SiegeChatSettings.channelListData[a].channelID == e then
					isActive = SiegeChatSettings.channelFiltersState[g]
					break
				end
			end
			-- TODO: May need to add LabelSetText(SiegeChatSettings.Window.."ListRow"..k.."ChannelName", SiegeChatSettings.channelListData[a].channelName)
			ButtonSetPressedFlag(SiegeChatSettings.Window.."ListRow"..k.."CheckBox", isActive)
			LabelSetTextColor( SiegeChatSettings.Window.."ListRow"..k.."ChannelName"
			                 , SiegeChatSettings.channelListData[a].color.r 
			                 , SiegeChatSettings.channelListData[a].color.g
			                 , SiegeChatSettings.channelListData[a].color.b
			                 )
		end
	end
end

function SiegeChatSettings.OnToggleChannel()
	local windowIndex = WindowGetId (SystemData.ActiveWindow.name)
	local windowParent = WindowGetParent (SystemData.ActiveWindow.name)
	local dataIndex = ListBoxGetDataIndex (windowParent, windowIndex)
	local index = 0
	
	-- Search for the index of the Channel we're altering
	for k, a in ipairs(ChatSettings.Ordering) do --idx=1, #ChatSettings.Ordering do
		if SiegeChatSettings.channelListData[dataIndex].channelID == a then
			index = k
			break
		end
	end
	-- Toggle the channel
	SiegeChatSettings.channelFiltersState[index] = not SiegeChatSettings.channelFiltersState[index]
	ButtonSetPressedFlag(SiegeChatSettings.Window.."ListRow"..windowIndex.."CheckBox", SiegeChatSettings.channelFiltersState[index])
end