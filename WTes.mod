<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="WTes" version="1.1" date="28/04/2010" >
		<Author name="Medikage" email="medikage@gmail.com" />
		<Description text="The Warhammer Tracker Enhancement System or WTes mod provides work arounds for a number of Warhammer UI defects that are in the bug tracker but have yet to be fixed." />
		<VersionSettings gameVersion="1.3.6" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Dependencies>
			<Dependency name="LibSlash" />
			<Dependency name="EA_MoraleWindow" />
			<Dependency name="EA_ChatWindow" />
			<Dependency name="EASystem_GlyphDisplay" />
			<Dependency name="EA_SiegeWeaponWindow" />
			<Dependency name="EA_BackpackWindow" />
			<Dependency name="EA_ScenarioLobbyWindow" />
		</Dependencies>
		<Files>
			<File name="SiegeChatSettings.xml" />
			<File name="WTes.xml" />
		</Files>
		<SavedVariables>
			<SavedVariable name="WTes.Settings" />
		</SavedVariables>
		<OnInitialize>
			<CallFunction name="WTes.Initialise" />
		</OnInitialize>
		<OnUpdate/>
		<OnShutdown>
			<CallFunction name="WTes.Shutdown" />
		</OnShutdown>
	</UiMod>
</ModuleFile>