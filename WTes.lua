local VERSION = 1.1

WTes = {}
WTes.Debug = {}
WTes.Debug.Enabled = false
WTes.Debug.Level   = 3
WTes.Window = "WTes"
WTes.Registered = false

WTes.SiegeChat = {}
WTes.SiegeChat.EnableCheckToggle = false
WTes.SiegeChat.Window      = WTes.Window.."SiegeChat"
WTes.SiegeChat.Enabled     = false

WTes.GlyphPos = {}
WTes.GlyphPos.EnableCheckToggle   = false
WTes.GlyphPos.Window        = WTes.Window.."GlyphPos"
WTes.GlyphPos.Enabled       = false
WTes.GlyphPos.TrackerWindow = "EA_Window_GlyphTracker"

WTes.MoraleClick = {}
WTes.MoraleClick.EnableCheckToggle = false
WTes.MoraleClick.Window      = WTes.Window.."MoraleClick"
WTes.MoraleClick.Original    = {}
WTes.MoraleClick.Enabled     = false

WTes.Backpack = {}
WTes.Backpack.EnableCheckToggle = false
WTes.Backpack.Window      = WTes.Window.."Backpack"
WTes.Backpack.Original    = {}
WTes.Backpack.Enabled     = false

WTes.ScenStartPos = {}
WTes.ScenStartPos.EnableCheckToggle = false
WTes.ScenStartPos.Window     = WTes.Window.."ScenStartPos"
WTes.ScenStartPos.Enabled    = false
WTes.ScenStartPos.ScenWindow = "EA_Window_ScenarioJoinPrompt"

function WTes.Initialise()
	WTes.DebugMsg(L"Entering Initialise", 3)
	-- Check to see if any options are saved on the users profile. If not set default values.
	if not WTes.Settings then
		WTes.Settings = {}
		-- Siege Chat settings
		WTes.Settings.SiegeChat = {}
		WTes.Settings.SiegeChat.Enabled = false
		WTes.Settings.SiegeChat.Channels = {
			  [SystemData.ChatLogFilters.SAY]              = true
			, [SystemData.ChatLogFilters.SHOUT]            = true
			, [SystemData.ChatLogFilters.TELL_RECEIVE]     = true
			, [SystemData.ChatLogFilters.GROUP]            = true
			, [SystemData.ChatLogFilters.BATTLEGROUP]      = true
			, [SystemData.ChatLogFilters.GUILD]            = true
			, [SystemData.ChatLogFilters.GUILD_OFFICER]    = true
			, [SystemData.ChatLogFilters.ALLIANCE]         = true
			, [SystemData.ChatLogFilters.ALLIANCE_OFFICER] = true
			, [SystemData.ChatLogFilters.REALM_WAR_T1]     = true
			, [SystemData.ChatLogFilters.REALM_WAR_T2]     = true
			, [SystemData.ChatLogFilters.REALM_WAR_T3]     = true
			, [SystemData.ChatLogFilters.REALM_WAR_T4]     = true
			, [SystemData.ChatLogFilters.CHANNEL_1]        = true
			, [SystemData.ChatLogFilters.CHANNEL_2]        = true
			                               }
		-- Glyph Window Positioning settings				
		WTes.Settings.GlyphPos = {}
		WTes.Settings.GlyphPos.Enabled = false
		WTes.Settings.GlyphPos.Alpha   = 0
		WTes.Settings.GlyphPos.Anchors = {}
		WTes.Settings.GlyphPos.Height  = 0
		WTes.Settings.GlyphPos.Scale   = 0
		WTes.Settings.GlyphPos.Width   = 0
		WTes.Settings.GlyphPos.Default = {}
		WTes.Settings.GlyphPos.Default.Set     = false
		WTes.Settings.GlyphPos.Default.Alpha   = 0
		WTes.Settings.GlyphPos.Default.Anchors = {}
		WTes.Settings.GlyphPos.Default.Height  = 0
		WTes.Settings.GlyphPos.Default.Scale   = 0
		WTes.Settings.GlyphPos.Default.Width   = 0
				
		-- Morale Window clickable settings
		WTes.Settings.MoraleClick = {}
		WTes.Settings.MoraleClick.Enabled = false
		-- Backpack valid icons settings
		WTes.Settings.Backpack = {}
		WTes.Settings.Backpack.Enabled = false		
		-- Set the verison
		WTes.Settings.Version = 1
	end
	
	if WTes.Settings.Version < 1.1 then
		-- Glyph Window Positioning settings
		WTes.Settings.ScenStartPos = {}
		WTes.Settings.ScenStartPos.Enabled = false
		WTes.Settings.ScenStartPos.Alpha   = 0
		WTes.Settings.ScenStartPos.Anchors = {}
		WTes.Settings.ScenStartPos.Height  = 0
		WTes.Settings.ScenStartPos.Scale   = 0
		WTes.Settings.ScenStartPos.Width   = 0
		WTes.Settings.ScenStartPos.Default = {}
		WTes.Settings.ScenStartPos.Default.Set     = false
		WTes.Settings.ScenStartPos.Default.Alpha   = 0
		WTes.Settings.ScenStartPos.Default.Anchors = {}
		WTes.Settings.ScenStartPos.Default.Height  = 0
		WTes.Settings.ScenStartPos.Default.Scale   = 0
		WTes.Settings.ScenStartPos.Default.Width   = 0
		WTes.Settings.Version = 1.1
	end
	
	WTes.Settings.Version = VERSION
	-- Register the slash command.
	LibSlash.RegisterSlashCmd("wtes", WTes.Slash)
	LibSlash.RegisterSlashCmd("wtesd", function(args) WTes.SlashDebug(args) end)

	-- Initialise the options window
	WTes.WindowInit()
	SiegeChatSettings.WindowInit()
	WTes.RegisterEvents()
	-- Track the loading of the end of the mods in order to complete initialisation
	if SystemData.LoadingData.isLoading then
		WTes.DebugMsg(L"Initialise: UI is still loading", 2)
		RegisterEventHandler(SystemData.Events.LOADING_END, "WTes.InitComplete")
		WTes.Registered = true
	else
		WTes.InitComplete()
	end
	WTes.DebugMsg(L"Leaving Initialise", 3)
end

function WTes.InitComplete()
	WTes.DebugMsg(L"Entering InitComplete", 3)
	WTes.ApplySelectedFixes()
	EA_ChatWindow.Print(L"WTes is initialised. Type /wtes for options")
	if WTes.Registered then
		UnregisterEventHandler(SystemData.Events.LOADING_END, "WTes.InitComplete")
	end
	WTes.DebugMsg(L"Leaving InitComplete", 3)
end

function WTes.RegisterEvents()
	WTes.DebugMsg(L"Entering RegisterEvents", 3)
	-- Register Glyph Window Positioning events
	LayoutEditor.RegisterEditCallback(WTes.GlyphPos.LayoutEditorUpdate)
	LayoutEditor.RegisterEditCallback(WTes.ScenStartPos.LayoutEditorUpdate)
	WTes.DebugMsg(L"Leaving RegisterEvents", 3)
end

function WTes.UnregisterEvents()
	WTes.DebugMsg(L"Entering UnregisterEvents", 3)
	WTes.DebugMsg(L"Leaving UnregisterEvents", 3)
end

function WTes.DebugMsg(str,lvl)
	-- If debug is enabled and the priority of the message is high enough then send a debug message
	if WTes.Debug.Enabled and lvl <= WTes.Debug.Level then
		d(towstring(str))
	end
end

function WTes.SlashDebug(args)
	WTes.DebugMsg(L"Entering SlashDebug", 3)
	local arg, argv = args:match("([a-z0-9]+)[ ]?(.*)")
	WTes.DebugMsg("SlashDebug: arg ["..arg.."] argv ["..argv.."]", 2)
	if arg == nil then
		WTes.Debug.Enabled = not WTes.Debug.Enabled
		return
	end
	-- Set to true to display debug messaWTes in the debug window (Note: type /debug in game to see this window and pWTess the button "Logs(off)")
	-- Set the debug level (4 = Update Ticks, 3 = Position, 2 = Setting values, 1 = Return values, 0 = Illegal flow catches)
	if arg == "on" then
		WTes.Debug.Enabled = true
	elseif arg == "off" then
		WTes.Debug.Enabled = false
	elseif arg == "1" then
		WTes.Debug.Enabled = true
		WTes.Debug.Level = 1
	elseif arg == "2" then
		WTes.Debug.Enabled = true
		WTes.Debug.Level = 2
	elseif arg == "3" then
		WTes.Debug.Enabled = true
		WTes.Debug.Level = 3
	elseif arg == "4" then
		WTes.Debug.Enabled = true
		WTes.Debug.Level = 4
	else
		WTes.DebugMsg("SlashDebug: Invalid Argument ["..arg.."]",0)
	end
	WTes.DebugMsg(L"Leaving SlashDebug", 3)
end

----------------------------------
---   Gui Specific Functions   ---
----------------------------------

function WTes.Slash()
	WTes.DebugMsg(L"Entering Slash", 3)
	WTes.ShowWindow()
	WTes.DebugMsg(L"Leaving Slash", 3)
end

function WTes.WindowInit()
	WTes.DebugMsg(L"Entering WindowInit", 3)
	-- Create the window and then hide it.
	CreateWindow(WTes.Window, true)
	WindowSetShowing(WTes.Window, false)
	-- Apply the general GUI settings
	ButtonSetText(WTes.Window.."SaveButton" , L"Save")
	ButtonSetText(WTes.Window.."CloseButton", L"Close")
	LabelSetText(WTes.Window.."TitleBarText", L"Warhammer Tracker Enhancement System v"..towstring(WTes.Settings.Version))
	-- Apply the Siege Chat GUI Settings
	ButtonSetText(WTes.SiegeChat.Window.."Button", L"Customise")
	LabelSetText (WTes.SiegeChat.Window.."Label" , L"Customise the channels in the Siege Chat Window")
	-- Apply the GlyphPos GUI Settings
	ButtonSetText(WTes.GlyphPos.Window.."Button", L"Reset")
	LabelSetText (WTes.GlyphPos.Window.."Label" , L"Persist changes to the Glyph window beyond logoff")
	-- Apply the MoraleClick GUI Settings
	LabelSetText (WTes.MoraleClick.Window.."Label", L"Morale is clickable through a false activation")
	-- Apply the Backpack GUI Settings
	LabelSetText (WTes.Backpack.Window.."Label", L"Backpack crafting window is always valid")
	-- Apply the ScenStartPos GUI Settings
	ButtonSetText(WTes.ScenStartPos.Window.."Button", L"Reset")
	LabelSetText (WTes.ScenStartPos.Window.."Label" , L"Alter the Scenario Start window via the LayoutEditor")
	WTes.DebugMsg(L"Leaving WindowInit", 3)
end

function WTes.ShowWindow()
	WTes.DebugMsg(L"Entering ShowWindow", 3)
	-- Set the SiegeChat gui elements to the latest values.
	WTes.SiegeChat.EnableCheckToggle = WTes.Settings.SiegeChat.Enabled
	ButtonSetPressedFlag(WTes.SiegeChat.Window.."CheckBox", WTes.SiegeChat.EnableCheckToggle)
	-- Set the GlyphPos gui elements to the latest values.
	WTes.GlyphPos.EnableCheckToggle = WTes.Settings.GlyphPos.Enabled
	ButtonSetPressedFlag(WTes.GlyphPos.Window.."CheckBox", WTes.GlyphPos.EnableCheckToggle)
	-- Set the MoraleClick gui elements to the latest values.
	WTes.MoraleClick.EnableCheckToggle = WTes.Settings.MoraleClick.Enabled
	ButtonSetPressedFlag(WTes.MoraleClick.Window.."CheckBox", WTes.MoraleClick.EnableCheckToggle)
	-- Set the Backpack gui elements to the latest values.
	WTes.Backpack.EnableCheckToggle = WTes.Settings.Backpack.Enabled
	ButtonSetPressedFlag(WTes.Backpack.Window.."CheckBox", WTes.Backpack.EnableCheckToggle)
	-- Set the ScenStartPos gui elements to the latest values.
	WTes.ScenStartPos.EnableCheckToggle = WTes.Settings.ScenStartPos.Enabled
	ButtonSetPressedFlag(WTes.ScenStartPos.Window.."CheckBox", WTes.ScenStartPos.EnableCheckToggle)
	-- Show the window
	WindowSetShowing(WTes.Window,true)
	WTes.DebugMsg(L"Leaving ShowWindow", 3)
end

function WTes.CloseWindow(flags, mouseX, mouseY)
	WTes.DebugMsg(L"Entering CloseWindow", 3)
	-- When the button is clicked, hide the WTes window.
	WindowSetShowing(WTes.Window,false)
	WTes.DebugMsg(L"Leaving CloseWindow", 3)
end

function WTes.SaveOptions(flags, mouseX, mouseY)
	WTes.DebugMsg(L"Entering SaveOptions", 3)
	if WTes.Settings.SiegeChat.Enabled ~= ButtonGetPressedFlag(WTes.SiegeChat.Window.."CheckBox") then
		WTes.Settings.SiegeChat.Enabled = not WTes.Settings.SiegeChat.Enabled
		WTes.SiegeChat.Fix(WTes.Settings.SiegeChat.Enabled)
	end
	if WTes.Settings.GlyphPos.Enabled ~= ButtonGetPressedFlag(WTes.GlyphPos.Window.."CheckBox") then
		WTes.Settings.GlyphPos.Enabled = not WTes.Settings.GlyphPos.Enabled
		WTes.GlyphPos.Fix(WTes.Settings.GlyphPos.Enabled)
	end
	if WTes.Settings.MoraleClick.Enabled ~= ButtonGetPressedFlag(WTes.MoraleClick.Window.."CheckBox") then
		WTes.Settings.MoraleClick.Enabled = not WTes.Settings.MoraleClick.Enabled
		WTes.MoraleClick.Fix(WTes.Settings.MoraleClick.Enabled)
	end
	if WTes.Settings.Backpack.Enabled ~= ButtonGetPressedFlag(WTes.Backpack.Window.."CheckBox") then
		WTes.Settings.Backpack.Enabled = not WTes.Settings.Backpack.Enabled
		WTes.Backpack.Fix(WTes.Settings.Backpack.Enabled)
	end	
	if WTes.Settings.ScenStartPos.Enabled ~= ButtonGetPressedFlag(WTes.ScenStartPos.Window.."CheckBox") then
		WTes.Settings.ScenStartPos.Enabled = not WTes.Settings.ScenStartPos.Enabled
		WTes.ScenStartPos.Fix(WTes.Settings.ScenStartPos.Enabled)
	end	
	WTes.DebugMsg(L"Leaving SaveOptions", 3)
end

-----------------------------
-- Warhammer Fix Functions --
-----------------------------

function WTes.ApplySelectedFixes()
	WTes.DebugMsg(L"Entering ApplySelectedFixes", 3)
	WTes.SiegeChat.Fix   (WTes.Settings.SiegeChat.Enabled)
	WTes.GlyphPos.Fix    (WTes.Settings.GlyphPos.Enabled)
	WTes.MoraleClick.Fix (WTes.Settings.MoraleClick.Enabled)
	WTes.Backpack.Fix    (WTes.Settings.Backpack.Enabled)
	WTes.ScenStartPos.Fix(WTes.Settings.ScenStartPos.Enabled)
	WTes.DebugMsg(L"Leaving ApplySelectedFixes", 3)
end

------------------------------------------
-- Fix Siege Chat Window Spam Functions --
------------------------------------------
-- Currently the siege chat window displays every single channel that has a type of "Chat".
-- Due to addons like State of the Realm, which uses one of these chat channels to send area status 
-- updates every second, the chat window just displays alot of information that the user deosn't wish
-- to see.  This fix reduces the channels down to the most common channels that players use to 
-- communicate whilst siegeing a keep, fort or city.

function WTes.SiegeChat.Fix(activate)
	WTes.DebugMsg(L"Entering FixSiegeChatWindow", 3)
	-- Check to see if the fix is already in the desired state, if so don't do anything and exit
	if WTes.SiegeChat.Enabled == activate then return end

	local siegeWindow = "SiegeWeaponGeneralFireWindowChatLogDisplay"
	if (activate) then
		WTes.DebugMsg(L"Applying Siege Chat Fix", 3)
		WTes.SiegeChat.Enabled = true
		for k, a in pairs( ChatSettings.Channels ) do
			if WTes.Settings.SiegeChat.Channels[a.id] then
				LogDisplaySetFilterState(siegeWindow, a.logName, a.id, true )
			else
				LogDisplaySetFilterState(siegeWindow, a.logName, a.id, false)
			end
		end
		LogDisplaySetFilterState(siegeWindow, "Chat", 6, false)
	else
		WTes.DebugMsg(L"Removing Siege Chat Fix", 3)
		WTes.SiegeChat.Enabled = false
		for k, a in pairs( ChatSettings.Channels ) do
			LogDisplaySetFilterState( siegeWindow, a.logName, a.id, true )
		end
    end
	WTes.DebugMsg(L"Leaving FixSiegeChatWindow", 3)
end

function WTes.SiegeChat.CheckToggle(flags, mouseX, mouseY)
	WTes.DebugMsg(L"Entering WTes.SiegeChat.EnableCheckToggle", 3)
	WTes.SiegeChat.EnableCheckToggle = not WTes.SiegeChat.EnableCheckToggle
	ButtonSetPressedFlag(WTes.SiegeChat.Window.."CheckBox",WTes.SiegeChat.EnableCheckToggle)
	WTes.DebugMsg(L"Leaving WTes.SiegeChat.EnableCheckToggle", 3)
end

--------------------------------------------
-- Fix Glyph Window Positioning Functions --
--------------------------------------------
-- Currently the Glyph window (EA_Window_GlyphTracker) does not have the xml entry "savesettings=true"
-- and therefore if it is moved by a user, the new location does not persist passed exiting the game.
-- Note: I don't have access to the GlyphDisplay.xml file and I can't find were to dynamically set a window's
-- savesettings option via the Warhammer Lua API. Instead I am hooking into the LayoutEditor Ending event
-- and saving the changes applied to the window in order to re-apply these settings at start up.

function WTes.GlyphPos.Fix(activate)
	WTes.DebugMsg(L"Entering GlyphPos.Fix", 3)
	-- Check to see if the fix is already in the desired state, if so don't do anything and exit
	if WTes.GlyphPos.Enabled == activate then return end
	if activate then
		WTes.GlyphPos.Enabled = true
		-- Only apply positions if the window actually exists.
		if DoesWindowExist(WTes.GlyphPos.TrackerWindow) then
			WTes.DebugMsg(L"Applying Glyph Window Fix", 3)
			if not WTes.Settings.GlyphPos.Default.Set then
				-- This is the first time we've run so get all of the default GlyphTracker window settings and don't apply anything as it would be the same
				WTes.GlyphPos.LayoutEditorUpdate(LayoutEditor.EDITING_END)
				WTes.Settings.GlyphPos.Default.Width  = WTes.Settings.GlyphPos.Width
				WTes.Settings.GlyphPos.Default.Height = WTes.Settings.GlyphPos.Height
				WTes.Settings.GlyphPos.Default.Alpha  = WTes.Settings.GlyphPos.Alpha
				WTes.Settings.GlyphPos.Default.Scale  = WTes.Settings.GlyphPos.Scale
				WTes.Settings.GlyphPos.Default.Anchors = {}
				for k, a in pairs(WTes.Settings.GlyphPos.Anchors) do
					WTes.Settings.GlyphPos.Default.Anchors[k] = {}
					WTes.Settings.GlyphPos.Default.Anchors[k].point         = a.point
					WTes.Settings.GlyphPos.Default.Anchors[k].relativePoint = a.relativePoint
					WTes.Settings.GlyphPos.Default.Anchors[k].relativeTo    = a.relativeTo
					WTes.Settings.GlyphPos.Default.Anchors[k].offsetX       = a.offsetX
					WTes.Settings.GlyphPos.Default.Anchors[k].offsetY       = a.offsetY
				end
				WTes.Settings.GlyphPos.Default.Set = true
			else
				WindowSetDimensions(WTes.GlyphPos.TrackerWindow, WTes.Settings.GlyphPos.Width, WTes.Settings.GlyphPos.Height)
				WindowSetAlpha(WTes.GlyphPos.TrackerWindow, WTes.Settings.GlyphPos.Alpha)
				WindowSetScale(WTes.GlyphPos.TrackerWindow, WTes.Settings.GlyphPos.Scale)
				WindowClearAnchors (WTes.GlyphPos.TrackerWindow)
				for k, a in pairs(WTes.Settings.GlyphPos.Anchors) do
					WindowAddAnchor (WTes.GlyphPos.TrackerWindow, a.point, a.relativeTo, a.relativePoint, a.offsetX, a.offsetY)
				end
			end
		else
			-- Notify user that Glyph window does not exist and the fix is disabled.
			EA_ChatWindow.Print(L"The Glyph window does not currently exist. No settings can be applied")
			WTes.GlyphPos.Enabled = false
		end
	else
		WTes.GlyphPos.Enabled = false
		-- Reset Windows attributes back to default.
		WTes.GlyphPos.Reset(nil, nil, nil)
	end
	WTes.DebugMsg(L"Leaving GlyphPos.Fix", 3)
end

function WTes.GlyphPos.LayoutEditorUpdate(status)
	WTes.DebugMsg(L"Entering GlyphPos.LayoutEditorUpdate", 3)
	if status == LayoutEditor.EDITING_END and WTes.GlyphPos.Enabled then
		WTes.Settings.GlyphPos.Width, WTes.Settings.GlyphPos.Height = WindowGetDimensions(WTes.GlyphPos.TrackerWindow)
		WTes.Settings.GlyphPos.Scale = WindowGetScale(WTes.GlyphPos.TrackerWindow)
		WTes.Settings.GlyphPos.Alpha = WindowGetAlpha(WTes.GlyphPos.TrackerWindow)
		WTes.Settings.GlyphPos.Anchors = {}
		local numAnchors = WindowGetAnchorCount(WTes.GlyphPos.TrackerWindow)
		if numAnchors > 0 then
			for index = 1, numAnchors do
				local point, relativePoint, relativeTo, offsetX, offsetY = WindowGetAnchor(WTes.GlyphPos.TrackerWindow, index)
				WTes.Settings.GlyphPos.Anchors[index] = {}
				WTes.Settings.GlyphPos.Anchors[index].point         = point
				WTes.Settings.GlyphPos.Anchors[index].relativePoint = relativePoint
				WTes.Settings.GlyphPos.Anchors[index].relativeTo    = relativeTo
				WTes.Settings.GlyphPos.Anchors[index].offsetX       = offsetX
				WTes.Settings.GlyphPos.Anchors[index].offsetY       = offsetY
			end
		end
	end
	WTes.DebugMsg(L"Leaving GlyphPos.LayoutEditorUpdate", 3)
end

function WTes.GlyphPos.Reset(flags, mouseX, mouseY)
	WTes.DebugMsg(L"Entering GlyphPos.Reset", 3)
	WindowSetDimensions(WTes.GlyphPos.TrackerWindow, WTes.Settings.GlyphPos.Default.Width, WTes.Settings.GlyphPos.Default.Height)
	WindowSetAlpha(WTes.GlyphPos.TrackerWindow, WTes.Settings.GlyphPos.Default.Alpha)	
	WindowSetScale(WTes.GlyphPos.TrackerWindow, WTes.Settings.GlyphPos.Default.Scale)	
	WindowClearAnchors (WTes.GlyphPos.TrackerWindow)	
	for k, a in pairs(WTes.Settings.GlyphPos.Default.Anchors) do	
		WindowAddAnchor(WTes.GlyphPos.TrackerWindow, a.point, a.relativeTo, a.relativePoint, a.offsetX, a.offsetY)	
	end	
	WTes.Settings.GlyphPos.Width  = WTes.Settings.GlyphPos.Default.Width
	WTes.Settings.GlyphPos.Height = WTes.Settings.GlyphPos.Default.Height
	WTes.Settings.GlyphPos.Alpha  = WTes.Settings.GlyphPos.Default.Alpha
	WTes.Settings.GlyphPos.Scale  = WTes.Settings.GlyphPos.Default.Scale
	WTes.Settings.GlyphPos.Anchors = {}
	for k, a in pairs(WTes.Settings.GlyphPos.Default.Anchors) do
		WTes.Settings.GlyphPos.Anchors[k] = {}
		WTes.Settings.GlyphPos.Anchors[k].point         = a.point
		WTes.Settings.GlyphPos.Anchors[k].relativePoint = a.relativePoint
		WTes.Settings.GlyphPos.Anchors[k].relativeTo    = a.relativeTo
		WTes.Settings.GlyphPos.Anchors[k].offsetX       = a.offsetX
		WTes.Settings.GlyphPos.Anchors[k].offsetY       = a.offsetY
	end
	WTes.DebugMsg(L"Leaving GlyphPos.Reset", 3)
end

function WTes.GlyphPos.CheckToggle(flags, mouseX, mouseY)
	WTes.DebugMsg(L"Entering WTes.GlyphPos.EnableCheckToggle", 3)
	WTes.GlyphPos.EnableCheckToggle = not WTes.GlyphPos.EnableCheckToggle
	ButtonSetPressedFlag(WTes.GlyphPos.Window.."CheckBox",WTes.GlyphPos.EnableCheckToggle)
	WTes.DebugMsg(L"Leaving WTes.GlyphPos.EnableCheckToggle", 3)
end

--------------------------------------------
-- Fix Morale Button false fire Functions --
--------------------------------------------
-- If a Morale false fires (i.e. where the morale activates on the client but upon informing the server, it turns
-- out the morale's conditions aren't met.  An example of this is a direct healing morale which is activated to heal a 
-- player but the player has died in the time it took for the information to be sent to the server), the cooldown 
-- animation is shown but the Morale abilities are not in cooldown.  
-- The bug is that the cooldown animation is not clickable which prevents the user from using the morale when the
-- cooldown animation is showing, even though morale isn't in cooldown.  
-- This fix simply enables the click to pass through the cooldown animation, enabling the morale to still be
-- used if the cooldown animation is running when the morale ability is still usable.
-- NOTE: This is not an exploit to ignore Morale Cooldown timers!!! The animation does not control the morale buttons
-- usability but rather just adds a visual representation.
function WTes.MoraleClick.Fix(activate)
	WTes.DebugMsg(L"Entering MoraleClick.Fix", 3)
	-- Check to see if the fix is already in the desired state, if so don't do anything and exit
	if WTes.MoraleClick.Enabled == activate then return end
	WTes.MoraleClick.Enabled = activate
	if WTes.MoraleClick.Enabled then
		-- Take a backup of the original function so that we can restore them if the mod is disabled.
		WTes.MoraleClick.Original.SetCooldownFlag = MoraleSystem.SetCooldownFlag
		-- Override the MoraleSystem SetCooldownFlag function, adding a HandleInput call for the windows
		MoraleSystem.SetCooldownFlag = function(...) WTes.MoraleClick.Original.SetCooldownFlag(...) WTes.MoraleClick.SetCooldownFlag(...) end
	elseif WTes.MoraleClick.Original.SetCooldownFlag ~= nil then
		-- Restore the MoraleSystem SetCooldownFlag function, back to it's original state.
		MoraleSystem.SetCooldownFlag = function(...) WTes.MoraleClick.Original.SetCooldownFlag(...) end
		WTes.MoraleClick.SetCooldownFlag()
	end
	WTes.DebugMsg(L"Leaving MoraleClick.Fix", 3)
end

function WTes.MoraleClick.SetCooldownFlag()
	if GetMoraleCooldown(1) == 0 or
	   GetMoraleCooldown(2) == 0 or
	   GetMoraleCooldown(3) == 0 or
	   GetMoraleCooldown(4) == 0 then
		MoraleSystem.needsCooldownUpdate = false
	end
end

function WTes.MoraleClick.CheckToggle(flags, mouseX, mouseY)
	WTes.DebugMsg(L"Entering MoraleClick.EnableCheckToggle", 3)
	WTes.MoraleClick.EnableCheckToggle = not WTes.MoraleClick.EnableCheckToggle
	ButtonSetPressedFlag(WTes.MoraleClick.Window.."CheckBox", WTes.MoraleClick.EnableCheckToggle)
	WTes.DebugMsg(L"Leaving MoraleClick.EnableCheckToggle", 3)
end

----------------------------------------
-- Fix Backpack (Icon View) Functions --
----------------------------------------
-- If you close the backpack on the crafting window and then exit the game, the
-- next time you open the backpack window, the majority of the crafting icons are not selectable.
-- The bug is that the backpack window size is set to the default and not expanded to the approriate
-- size based on the users crafting rank. 
function WTes.Backpack.Fix(activate)
	WTes.DebugMsg(L"Entering Backpack.Fix", 3)
	-- Check to see if the fix is already in the desired state, if so don't do anything and exit
	if WTes.Backpack.Enabled == activate then return end
	WTes.Backpack.Enabled = activate
	if WTes.Backpack.Enabled then
		-- Take a backup of the original function so that we can restore them if the mod is disabled.
		WTes.Backpack.Original.OnShown = EA_Window_Backpack.OnShown
		-- Override the Backpack show function, adding a resizing call
		EA_Window_Backpack.OnShown = function(...) WTes.Backpack.Original.OnShown(...) WTes.Backpack.OnShown(...) end
	elseif WTes.Backpack.Original.OnShown ~= nil then
		-- Restore the Backpack show function, back to it's original state.
		EA_Window_Backpack.OnShown = function(...) WTes.Backpack.Original.OnShown(...) end
	end
	WTes.DebugMsg(L"Leaving Backpack.Fix", 3)
end

function WTes.Backpack.OnShown()
	WindowSetDimensions( EA_Window_Backpack.windowName,EA_Window_Backpack.views[EA_Window_Backpack.currentMode].mainWindowWidth, EA_Window_Backpack.views[EA_Window_Backpack.currentMode].mainWindowHeight)
end

function WTes.Backpack.CheckToggle(flags, mouseX, mouseY)
	WTes.DebugMsg(L"Entering Backpack.EnableCheckToggle", 3)
	WTes.Backpack.EnableCheckToggle = not WTes.Backpack.EnableCheckToggle
	ButtonSetPressedFlag(WTes.Backpack.Window.."CheckBox", WTes.Backpack.EnableCheckToggle)
	WTes.DebugMsg(L"Leaving Backpack.EnableCheckToggle", 3)
end

--------------------------------------------------------
-- Fix Scenario Starting Window Positioning Functions --
--------------------------------------------------------
-- Currently the Scenario Starting window (EA_Window_ScenarioStarting) has the xml entry "savesettings=false"
-- and therefore if it is moved by a user, the new location does not persist past exiting the game.
-- Note: I don't have access to the scenariolobbywindow.xml file and I can't find were to dynamically set a window's
-- savesettings option via the Warhammer Lua API. Instead I am registering the window with the LayoutEditor and then
-- hooking into the LayoutEditor Ending event to save the changes applied to the window in order to re-apply 
-- these settings at start up.

function WTes.ScenStartPos.Fix(activate)
	WTes.DebugMsg(L"Entering ScenStartPos.Fix", 3)
	-- Check to see if the fix is already in the desired state, if so don't do anything and exit
	if WTes.ScenStartPos.Enabled == activate then return end
	if activate then
		-- Only apply positions if the window actually exists.
		if DoesWindowExist(WTes.ScenStartPos.ScenWindow) then
			WTes.ScenStartPos.Enabled = true
			LayoutEditor.RegisterWindow( WTes.ScenStartPos.ScenWindow
									   , towstring(WTes.ScenStartPos.ScenWindow)
								       , towstring(WTes.ScenStartPos.ScenWindow)
								       , true, true, true, nil
								       )
		    if not WTes.Settings.ScenStartPos.Default.Set then
				-- This is the first time we've run so get all of the default Scenario Starting window settings and don't apply anything as it would be the same
				WTes.ScenStartPos.LayoutEditorUpdate(LayoutEditor.EDITING_END)
				WTes.Settings.ScenStartPos.Default.Width  = WTes.Settings.ScenStartPos.Width
				WTes.Settings.ScenStartPos.Default.Height = WTes.Settings.ScenStartPos.Height
				WTes.Settings.ScenStartPos.Default.Alpha  = WTes.Settings.ScenStartPos.Alpha
				WTes.Settings.ScenStartPos.Default.Scale  = WTes.Settings.ScenStartPos.Scale
				WTes.Settings.ScenStartPos.Default.Anchors = {}
				for k, a in pairs(WTes.Settings.ScenStartPos.Anchors) do
					WTes.Settings.ScenStartPos.Default.Anchors[k] = {}
					WTes.Settings.ScenStartPos.Default.Anchors[k].point         = a.point
					WTes.Settings.ScenStartPos.Default.Anchors[k].relativePoint = a.relativePoint
					WTes.Settings.ScenStartPos.Default.Anchors[k].relativeTo    = a.relativeTo
					WTes.Settings.ScenStartPos.Default.Anchors[k].offsetX       = a.offsetX
					WTes.Settings.ScenStartPos.Default.Anchors[k].offsetY       = a.offsetY
				end
				WTes.Settings.ScenStartPos.Default.Set = true
			else
				WindowSetDimensions(WTes.ScenStartPos.ScenWindow, WTes.Settings.ScenStartPos.Width, WTes.Settings.ScenStartPos.Height)
				WindowSetAlpha(WTes.ScenStartPos.ScenWindow, WTes.Settings.ScenStartPos.Alpha)
				WindowSetScale(WTes.ScenStartPos.ScenWindow, WTes.Settings.ScenStartPos.Scale)
				WindowClearAnchors (WTes.ScenStartPos.ScenWindow)
				for k, a in pairs(WTes.Settings.ScenStartPos.Anchors) do
					WindowAddAnchor(WTes.ScenStartPos.ScenWindow, a.point, a.relativeTo, a.relativePoint, a.offsetX, a.offsetY)
				end
			end
		else
			-- Notify user that Scenario Starting window does not exist and the fix is disabled.
			EA_ChatWindow.Print(L"The Scenario Starting window does not currently exist. No settings can be applied")
			WTes.ScenStartPos.Enabled = false
		end
	else
		WTes.ScenStartPos.Enabled = false
		if DoesWindowExist(WTes.ScenStartPos.ScenWindow) then
			LayoutEditor.UnregisterWindow(WTes.ScenStartPos.ScenWindow)
		end
		-- Reset Windows attributes back to default.
		WTes.ScenStartPos.Reset(nil, nil, nil)
	end
	WTes.DebugMsg(L"Leaving ScenStartPos.Fix", 3)
end

function WTes.ScenStartPos.LayoutEditorUpdate(status)
	WTes.DebugMsg(L"Entering ScenStartPos.LayoutEditorUpdate", 3)
	if status == LayoutEditor.EDITING_END and WTes.ScenStartPos.Enabled then
		WTes.Settings.ScenStartPos.Width, WTes.Settings.ScenStartPos.Height = WindowGetDimensions(WTes.ScenStartPos.ScenWindow)
		WTes.Settings.ScenStartPos.Scale = WindowGetScale(WTes.ScenStartPos.ScenWindow)
		WTes.Settings.ScenStartPos.Alpha = WindowGetAlpha(WTes.ScenStartPos.ScenWindow)
		WTes.Settings.ScenStartPos.Anchors = {}
		local numAnchors = WindowGetAnchorCount(WTes.ScenStartPos.ScenWindow)
		if numAnchors > 0 then
			for index = 1, numAnchors do
				local point, relativePoint, relativeTo, offsetX, offsetY = WindowGetAnchor(WTes.ScenStartPos.ScenWindow, index)
				WTes.Settings.ScenStartPos.Anchors[index] = {}
				WTes.Settings.ScenStartPos.Anchors[index].point         = point
				WTes.Settings.ScenStartPos.Anchors[index].relativePoint = relativePoint
				WTes.Settings.ScenStartPos.Anchors[index].relativeTo    = relativeTo
				WTes.Settings.ScenStartPos.Anchors[index].offsetX       = offsetX
				WTes.Settings.ScenStartPos.Anchors[index].offsetY       = offsetY
			end
		end
	end
	WTes.DebugMsg(L"Leaving ScenStartPos.LayoutEditorUpdate", 3)
end

function WTes.ScenStartPos.Reset(flags, mouseX, mouseY)
	WTes.DebugMsg(L"Entering ScenStartPos.Reset", 3)
	WindowSetDimensions(WTes.ScenStartPos.ScenWindow, WTes.Settings.ScenStartPos.Default.Width, WTes.Settings.ScenStartPos.Default.Height)
	WindowSetAlpha(WTes.ScenStartPos.ScenWindow, WTes.Settings.ScenStartPos.Default.Alpha)	
	WindowSetScale(WTes.ScenStartPos.ScenWindow, WTes.Settings.ScenStartPos.Default.Scale)	
	WindowClearAnchors (WTes.ScenStartPos.ScenWindow)	
	for k, a in pairs(WTes.Settings.ScenStartPos.Default.Anchors) do	
		WindowAddAnchor(WTes.ScenStartPos.ScenWindow, a.point, a.relativeTo, a.relativePoint, a.offsetX, a.offsetY)	
	end	
	WTes.Settings.ScenStartPos.Width  = WTes.Settings.ScenStartPos.Default.Width
	WTes.Settings.ScenStartPos.Height = WTes.Settings.ScenStartPos.Default.Height
	WTes.Settings.ScenStartPos.Alpha  = WTes.Settings.ScenStartPos.Default.Alpha
	WTes.Settings.ScenStartPos.Scale  = WTes.Settings.ScenStartPos.Default.Scale
	WTes.Settings.ScenStartPos.Anchors = {}
	for k, a in pairs(WTes.Settings.ScenStartPos.Default.Anchors) do
		WTes.Settings.ScenStartPos.Anchors[k] = {}
		WTes.Settings.ScenStartPos.Anchors[k].point         = a.point
		WTes.Settings.ScenStartPos.Anchors[k].relativePoint = a.relativePoint
		WTes.Settings.ScenStartPos.Anchors[k].relativeTo    = a.relativeTo
		WTes.Settings.ScenStartPos.Anchors[k].offsetX       = a.offsetX
		WTes.Settings.ScenStartPos.Anchors[k].offsetY       = a.offsetY
	end
	WTes.DebugMsg(L"Leaving ScenStartPos.Reset", 3)
end

function WTes.ScenStartPos.CheckToggle(flags, mouseX, mouseY)
	WTes.DebugMsg(L"Entering WTes.ScenStartPos.EnableCheckToggle", 3)
	WTes.ScenStartPos.EnableCheckToggle = not WTes.ScenStartPos.EnableCheckToggle
	ButtonSetPressedFlag(WTes.ScenStartPos.Window.."CheckBox",WTes.ScenStartPos.EnableCheckToggle)
	WTes.DebugMsg(L"Leaving WTes.ScenStartPos.EnableCheckToggle", 3)
end

